app.config(function($routeProvider) {
    
   
        $routeProvider

            // route for the home page
            .when('/', {
                templateUrl : 'app/templates/home.html',
                controller  : 'HomeController'
            })

            // route for the about page
            .when('/movies', {
                templateUrl : 'app/templates/movies.html',
                controller  : 'MovieController'
            })

            // route for the contact page
            .when('/movies/:movieId', {
                templateUrl : 'app/templates/moviedetail.html',
                controller  : 'MovieDetailController'
            })
            
            
             // route for the about page
            .when('/search', {
                templateUrl : 'app/templates/search.html',
                controller  : 'SearchController'
            })
            
             .when('/booked', {
                templateUrl : 'app/templates/booked.html',
                controller  : 'BookedController'
            })
 // route for the about page
            .when('/booking/:movieId', {
                templateUrl : 'app/templates/booking.html',
                controller  : 'BookingController'
            })

            
            ;
    });