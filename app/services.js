app.factory('BookingService',function () {
	
	var bookingData = {"name" : "", "row" : "", "col" : ""};
	

	
	return {
    
		setBooking : function (name) {
		
		
			bookingData.name = name;
		
		}, 
		
		get : function () {
			
			return bookingData;
		}
	
	}
	
	
})
app.factory('CinemaService', function() {
	
	 var cinema = [{
    		id: 0,
   			rows: 10,
   			cols: 10,
			reservations : [[0,0],[0,1],[0,2],[1,0],[2,1],[3,4]]
			   
			   
    
  	},
	  {
    		id: 1,
   			rows: 15,
   			cols: 15,
    		reservations : [[1,0],[2,1],[3,4]]
  		},
	  
	    {
    		id: 2,
   			rows: 20,
   			cols: 20,
			reservations : [[1,0],[2,1],[3,4]]
    
  		},

	  ];
	
	return {
    
		all : function () {
		
			return cinema;
		
		}, 
		
		get : function (id) {
			
			return cinema[id];
		}
		
		,
		
		
		boolSeatReserved : function (id,row,col) {
			
			var result = false;
			
			
			
			for(var n in cinema[id].reservations) {
				var reservedRow = cinema[id].reservations[n][0];
				var reservedCol = cinema[id].reservations[n][1];
				
				if (row == reservedRow && col == reservedCol) {
					console.log("Matching found");
					result = true;
					break;
				
				}
				
				
				
			}
			
			return result;
			
		}
		
		
		
		
	
	}
	
});

app.factory('MovieService', function() {
	
	 var movies = [{
    		id: 0,
   			title: 'Jaws',
   			director: 'Stephen Spielberg',
			cinema: 0,
			showTimes: ["13:00","14:00","15:00"]
    
  	},
	  {
    		id: 1,
   			title: 'Batman',
   			director: 'Christoper Nolan',
			cinema: 1,
			showTimes: ["12:00","16:00","18:00"]
    
  		},
	  
	    {
    		id: 2,
   			title: 'Spectre',
   			director: 'Sam Mendes',
			cinema: 2,
			showTimes: ["19:00","20:00","21:00"]
    
  		},

	  ];
	
	return {
    
		all : function () {
		
			return movies;
		
		}, 
		
		get : function (id) {
			
			return movies[id];
		}
	
	}
	
	
});