app.controller('HomeController', function ($scope, MovieService){ 
	
	$scope.movies = MovieService.all();
	
	
	
});

app.controller('MovieController', function ($scope){ 
	
	
	
	
});

app.controller('BookedController', function ($scope,BookingService){ 
	
	
	$scope.bookingData = BookingService.get();
	
});


app.controller('BookingController', function ($scope, $routeParams, MovieService, CinemaService, BookingService){ 
	
	$scope.movieId = $routeParams.movieId;
	$scope.movie = MovieService.get( $scope.movieId );
	
	$scope.isBooked = 0;
	//console.log($scope.movie);
	
	$scope.numSeats = 2;
	$scope.numSeatsChosen = 0;
	
	$scope.cinema = CinemaService.get($scope.movie.cinema);
	$scope.selectedShowTime = "";
	$scope.bookingData = BookingService.get();
	
	$scope.rows = [];
	$scope.cols = [];
	
	for(var i = 0; i < $scope.cinema.rows ; i++) {
		
		$scope.rows.push(i);

	}
	
	for(var i = 0; i < $scope.cinema.cols ; i++) {
		
		$scope.cols.push(i);

	}
	
	
	$scope.demo ="Testing";
	
	$scope.createBooking = function () {
		console.log("Booking is being made");
		$scope.isBooked = 1;
		$scope.bookingData.row = $scope.rowChosen
		$scope.bookingData.col = $scope.colChosen
		
		$scope.cinema.reservations.push([$scope.rowChosen,$scope.colChosen ]);
		
		//BookingService.setBooking("new booking name");
		
	}
	
	$scope.chooseSeat = function(obj) {
		
		var row = $scope.rowChosen = obj.$parent.$index;
		var col = $scope.colChosen = obj.$index;
		
		var chosenSeat = "#seat_" + $scope.rowChosen + "_" + $scope.colChosen;
		
		if (! CinemaService.boolSeatReserved($scope.movie.cinema, row,col)) {
			
			if ( $(chosenSeat).hasClass("selected")) {
				$scope.rowChosen ="";
				$scope.colChosen ="";
				$(chosenSeat).removeClass("selected");
				$scope.numSeatsChosen--;
				return;
				
			}
			
			
			if ($scope.numSeatsChosen < $scope.numSeats) {
			
			//$(".seat").removeClass("selected");
			
			$(chosenSeat).addClass("selected");
			$scope.numSeatsChosen++
			}
			
		}
		
		
		
		
		
	}
	
	$scope.addClass = function (obj) {
		
		var classNames = "seat";
		
		var row = obj.$parent.$index;
		var col = obj.$index;
		
		//console.log($scope.cinema.reservations);
		
		for(var n in $scope.cinema.reservations) {
			var reservedRow = $scope.cinema.reservations[n][0];
			var reservedCol = $scope.cinema.reservations[n][1];
			//console.log("Checking ", $scope.cinema.reservations[n][0] )
			if (row == reservedRow && col == reservedCol) {
				
				classNames += " reserve";
				break;
				return classNames;
				
			}
			
			
			
		}
		
		
		return classNames;
		
	}
	
});

app.controller('SearchController', function ($scope, $http){ 
	
	$scope.searchItem = "";
	
	$scope.resultFound = false;
	
	$scope.search = function () {
		
		$scope.resultFound = false;
	var url="http://www.omdbapi.com/?t=" + $scope.searchItem + "&y=&plot=short&r=json";
	$http.get(url)
    .success(function(response) {
		
		//console.log("resonse");
		
		$scope.movie = response;
		
		$scope.resultFound = true;
		
		});
		
		
		
		
		
		
	};
	
	
});

app.controller('MovieDetailController', function ($scope, $routeParams,MovieService){ 
	
	
	$scope.movieId = $routeParams.movieId;
	$scope.movie = MovieService.get( $scope.movieId );
	
	
});

